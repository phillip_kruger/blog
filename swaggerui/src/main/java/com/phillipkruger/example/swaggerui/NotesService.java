package com.phillipkruger.example.swaggerui;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.validation.constraints.NotNull;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 * Service for reminder notes
 * @author Phillip Kruger (phillip.kruger@gmail.com)
 */
@Api(value = "Notes service")
@Path("/note")
@Produces(MediaType.APPLICATION_JSON) @Consumes(MediaType.APPLICATION_JSON)
public class NotesService {
    
    private static final Map<String,Note> NOTE_DB = new HashMap<>(); 
    
    @POST
    @Path("/{title}")
    @ApiOperation(value = "Create a new note", notes = "This will create a new note under a certain title, only if it does not exist already")
    public Note createNote(@NotNull @PathParam("title") String title,@NotNull String text) throws NoteExistAlreadyException{
        if(exist(title))throw new NoteExistAlreadyException();
        Note note = new Note(title, text);
        save(note);
        return note;
    }
    
    @GET
    @Path("/{title}")
    @ApiOperation(value = "Retrieve a note", notes = "This will find the note with a certain title, if it exist")
    public Note getNote(@NotNull @PathParam("title") String title) throws NoteNotFoundException{
        if(!exist(title))throw new NoteNotFoundException();
        return NOTE_DB.get(title);
    }
    
    @DELETE
    @Path("/{title}")
    @ApiOperation(value = "Delete a note", notes = "This will delete the note with a certain title, if it exist")
    public void deleteNote(@NotNull @PathParam("title") String title){
        if(exist(title)){
            NOTE_DB.remove(title);
        }
    }
    
    @PUT
    @ApiOperation(value = "Update a note", notes = "This will update the note with a given new note, if it exist")
    public Note updateNote(@NotNull Note note) throws NoteNotFoundException{
        if(!exist(note.getTitle()))throw new NoteNotFoundException();
        save(note);
        return note;
    }
    
    @GET
    @Path("/exists/{title}")
    @ApiOperation(value = "Check if note exist", notes = "This will check if a note with a certain title exist")
    public boolean exist(@NotNull @PathParam("title") String title){
        return NOTE_DB.containsKey(title);
    }
    
    @GET
    @ApiOperation(value = "Get all the note titles", notes = "This will return all current note titles")
    public List<String> getNoteTitles(){
        return new ArrayList<>(NOTE_DB.keySet());
    }
    
    private void save(Note note){
        NOTE_DB.put(note.getTitle(), note);
    }

}