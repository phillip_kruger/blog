package com.phillipkruger.example.swaggerui;

import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;
import lombok.extern.java.Log;

@Provider
@Log
public class NoteExistAlreadyExceptionMapper implements ExceptionMapper<NoteExistAlreadyException> {
    
    @Override
    @Produces(MediaType.APPLICATION_JSON)
    public Response toResponse(NoteExistAlreadyException noteExistAlreadyException) {
        return Response.status(Response.Status.CONFLICT).entity(noteExistAlreadyException.getMessage()).build();
    }
}