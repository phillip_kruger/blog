package com.phillipkruger.example.hazelcast.analytics;

import com.phillipkruger.example.hazelcast.noteslistener.Note;
import javax.ejb.Stateless;
import javax.enterprise.event.Observes;
import lombok.extern.java.Log;

/**
 * Auditing note movement
 * @author Phillip Kruger (phillip.kruger@momentum.co.za)
 */
@Log
@Stateless
public class AnalyticsContributor {

    public void contribute(@Observes Note note){
        log.severe(">>>>>>> ANALYTICS: " + note);
    }
    
}
