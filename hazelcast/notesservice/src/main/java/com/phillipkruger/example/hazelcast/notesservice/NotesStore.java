package com.phillipkruger.example.hazelcast.notesservice;

import com.phillipkruger.example.hazelcast.noteslistener.Note;
import com.hazelcast.core.IMap;
import com.hazelcast.core.ITopic;
import com.phillipkruger.example.hazelcast.noteslistener.HazelcastConnectionFactory;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.validation.constraints.NotNull;


/**
 * Service for storing notes
 * @author Phillip Kruger (phillip.kruger@gmail.com)
 */
@Stateless
public class NotesStore {
    
    private IMap<String, Note> store; 
    
    private ITopic<byte[]> topic;
    
    @EJB
    private HazelcastConnectionFactory connectionFactory;
    
    @PostConstruct
    public void init() {
        this.store = connectionFactory.getStore();
        this.topic = connectionFactory.getTopic();
    }
    
    public Note get(String title){
        return store.get(title);
    }
    
    public void remove(String title){
        store.remove(title);
    }
    
    public void save(@NotNull Note note){
        store.put(note.getTitle(), note); 
        topic.publish(note.marshal());// http://docs.hazelcast.org/docs/3.5/manual/html/serialization.html
    }
    
    public boolean containsTitle(String title){
        return store.containsKey(title);
    }
    
    public List<String> keys(){
        Set<String> keySet = new HashSet<>(store.keySet());
        return new ArrayList<>(keySet);
    }
    
    public int size(){
        return store.size();
    }
    
}