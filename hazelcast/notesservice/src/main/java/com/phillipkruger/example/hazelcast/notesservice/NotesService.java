package com.phillipkruger.example.hazelcast.notesservice;

import com.phillipkruger.example.hazelcast.noteslistener.Note;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.validation.constraints.NotNull;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 * Service for reminder notes
 * @author Phillip Kruger (phillip.kruger@gmail.com)
 */
@Api(value = "Notes service")
@Path("/note")
@Produces(MediaType.APPLICATION_JSON) @Consumes(MediaType.APPLICATION_JSON)
@Stateless
public class NotesService {
    
    @EJB
    private NotesStore store;
    
    @POST
    @Path("/{title}")
    @ApiOperation(value = "Create a new note", notes = "This will create a new note under a certain title, only if it does not exist already")
    public Note createNote(@NotNull @PathParam("title") String title,@NotNull String text) throws NoteExistAlreadyException{
        if(exist(title))throw new NoteExistAlreadyException();
        Note note = new Note(title, text);
        store.save(note);
        return note;
    }
    
    @GET
    @Path("/{title}")
    @ApiOperation(value = "Retrieve a note", notes = "This will find the note with a certain title, if it exist")
    public Note getNote(@NotNull @PathParam("title") String title) throws NoteNotFoundException{
        if(!exist(title))throw new NoteNotFoundException();
        return store.get(title);
    }
    
    @DELETE
    @Path("/{title}")
    @ApiOperation(value = "Delete a note", notes = "This will delete the note with a certain title, if it exist")
    public void deleteNote(@NotNull @PathParam("title") String title){
        if(exist(title)){
            store.remove(title);
        }
    }
    
    @PUT
    @ApiOperation(value = "Update a note", notes = "This will update the note with a given new note, if it exist")
    public Note updateNote(@NotNull Note note) throws NoteNotFoundException{
        if(!exist(note.getTitle()))throw new NoteNotFoundException();
        store.save(note);
        return note;
    }
    
    @GET
    @Path("/exists/{title}")
    @ApiOperation(value = "Check if note exist", notes = "This will check if a note with a certain title exist")
    public boolean exist(@NotNull @PathParam("title") String title){
        return store.containsTitle(title);
    }
    
    @GET
    @ApiOperation(value = "Get all the note titles", notes = "This will return all current note titles")
    public List<String> getNoteTitles(){
        List<String> notes = store.keys();
        if(notes==null || notes.isEmpty())return null;
        return notes;
    }
 
    @GET
    @Path("/size")
    @ApiOperation(value = "Get the number of notes", notes = "This will return the number of stored notes")
    public int getNumberOfNotes(){
        return store.size();
    }
    
}