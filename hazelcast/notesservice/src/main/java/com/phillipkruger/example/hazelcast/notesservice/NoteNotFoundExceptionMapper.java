package com.phillipkruger.example.hazelcast.notesservice;

import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;
import lombok.extern.java.Log;

@Provider
@Log
public class NoteNotFoundExceptionMapper implements ExceptionMapper<NoteNotFoundException> {
    
    @Override
    @Produces(MediaType.APPLICATION_JSON)
    public Response toResponse(NoteNotFoundException noteNotFoundException) {
        return Response.status(Response.Status.NOT_FOUND).entity(noteNotFoundException.getMessage()).build();
    }
}