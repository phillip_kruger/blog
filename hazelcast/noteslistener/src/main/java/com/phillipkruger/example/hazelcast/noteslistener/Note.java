package com.phillipkruger.example.hazelcast.noteslistener;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.util.Date;
import javax.validation.constraints.NotNull;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data @NoArgsConstructor @AllArgsConstructor
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Note implements Serializable {
    private static final long serialVersionUID = -8531040143398373846L;
    
    @NotNull @XmlAttribute(required=true) 
    private Date created = new Date();
    @NotNull @XmlAttribute(required=true) 
    private Date lastUpdated = new Date();
    @NotNull @XmlAttribute(required=true) 
    private String title;
    @NotNull @XmlAttribute(required=true) 
    private String text;
    
    public Note(@NotNull String title, @NotNull String text){
        this.created = new Date();
        this.lastUpdated = new Date();
        this.title = title;
        this.text = text;
    }
    
    
    public byte[] marshal(){
        try (ByteArrayOutputStream baos = new ByteArrayOutputStream()){
            Marshaller m = JAXB_CONTEXT.createMarshaller();
            m.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
            m.marshal(this, baos);
            baos.flush();
            return baos.toByteArray();
        } catch (JAXBException | IOException ex) {
            throw new RuntimeException(ex.getMessage());
        }
    }
    
    public static Note unmarshal(byte[] bytes) {
        try(ByteArrayInputStream bais = new ByteArrayInputStream(bytes)){
            Unmarshaller u = JAXB_CONTEXT.createUnmarshaller();
            return (Note) u.unmarshal(bais);
        } catch (JAXBException | IOException ex) {
            throw new RuntimeException(ex.getMessage());
        }
        
    } 
    
    static{
        try {
            JAXB_CONTEXT = JAXBContext.newInstance(Note.class);
        } catch (JAXBException ex) {
            throw new RuntimeException(ex);
        }
    }
    private static final JAXBContext JAXB_CONTEXT;
}
