package com.phillipkruger.example.hazelcast.noteslistener;

import com.hazelcast.core.IMap;
import com.hazelcast.core.ITopic;
import com.hazelcast.jca.HazelcastConnection;
import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.resource.ResourceException;
import javax.resource.cci.ConnectionFactory;
import lombok.Getter;
import lombok.extern.java.Log;


/**
 * Service to establish the Hazelcast connection and make the topic and cache available
 * @author Phillip Kruger (phillip.kruger@gmail.com)
 */
@Log
@Stateless
public class HazelcastConnectionFactory {

    @Resource(lookup = FACTORY_JNDI) 
    private ConnectionFactory connectionFactory;
    
    @Getter
    private ITopic<byte[]> topic;
    @Getter
    private IMap<String, Note> store; 
    
    private HazelcastConnection connection = null;
    
    @PostConstruct
    protected void init(){
        try {
            this.connection = (HazelcastConnection) connectionFactory.getConnection();
            
            this.topic = connection.getTopic(NAME);
            this.store = connection.getMap(NAME);
        } catch (ResourceException ex) {
            throw new RuntimeException(ex);
        }
    }
    
    private static final String NAME = "notes";
    private static final String FACTORY_JNDI = "HazelcastCF";
}