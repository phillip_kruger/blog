package com.phillipkruger.example.hazelcast.noteslistener;

import com.hazelcast.core.ITopic;
import com.hazelcast.core.Message;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.EJB;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.enterprise.event.Event;
import javax.inject.Inject;
import lombok.extern.java.Log;


/**
 * Service for receiving event on notes
 * @author Phillip Kruger (phillip.kruger@gmail.com)
 */
@Log
@Singleton @Startup
public class NotesEventReceiver {

    @EJB
    private HazelcastConnectionFactory connectionFactory;
    
    @Inject Event<Note> broadcaster;
    
    private String receiverName = null;
    
    private ITopic<byte[]> topic;
    
    @PostConstruct
    public void init(){
        this.topic = connectionFactory.getTopic();
        registerTopicListener();
    }
    
    @PreDestroy
    public void shutdown(){
        this.topic.removeMessageListener(this.receiverName);
        receiverName = null;
        topic = null;
    }
    
    private void registerTopicListener(){
        // Add filter ?
        
        this.receiverName = this.topic.addMessageListener((Message<byte[]> message) -> {
            byte[] bytes = message.getMessageObject();
            broadcaster.fire(Note.unmarshal(bytes));
        });
        
    }
}